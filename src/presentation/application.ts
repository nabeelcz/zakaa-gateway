import express, { Application } from "express";
import { setupLogging } from "@/_lib/logging";
import { setupProxies } from "@/_lib/proxy";
import cors from "cors";

const app: Application = express();

const corsOptions = {
    origin: process.env.CLIENT_URL || 'http://localhost:3000',
    methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
    credentials: true,
}

app.use(cors(corsOptions));
setupLogging(app);
setupProxies(app, [
    {
        url: '/api/auth',
        proxy: {
            target: process.env.AUTH_SERVICE || "http://localhost:3001",
            changeOrigin: true,
        }
    }, 
    {
        url: '/api/user',
        proxy: {
            target: process.env.USER_SERVICE || "http://localhost:3002",
            changeOrigin: true,
        }
    },
    {
        url: '/api/notification',
        proxy: {
            target: process.env.NOTIFICATION_SERVICE || "http://localhost:3003",
            changeOrigin: true,
        }
    },
    {
        url: '/api/course',
        proxy: {
            target: process.env.COURSE_SERVICE || "http://localhost:3004",
            changeOrigin: true,
        }
    },
    {
        url: '/api/payment',
        proxy: {
            target: process.env.PAYMENT_SERVICE || "http://localhost:3005",
            changeOrigin: true,
        }
    },
    {
        url: '/api/chat',
        proxy: {
            target: process.env.CHAT_SERVICE || "http://localhost:3006",
            changeOrigin: true,
        }
    }
]);

export default app;